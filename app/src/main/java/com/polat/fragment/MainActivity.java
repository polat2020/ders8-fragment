package com.polat.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.polat.fragment.Fragment.FirstFragment;
import com.polat.fragment.Fragment.SecondFragment;
import com.polat.fragment.Fragment.ThirdFragment;

public class MainActivity extends AppCompatActivity {

    MainViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mViewModel = new ViewModelProvider(this).get(MainViewModel.class);

    }

    public void btn1Click(View view) {
        getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout,
                FirstFragment.newInstance()).commit();
    }


    public void btn2Click(View view) {
        EditText txt = findViewById(R.id.txtInput);
        String name = txt.getText().toString();

        getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout,
                SecondFragment.newInstance(name)).commit();
    }

    public void btn3Click(View view) {
        EditText txt = findViewById(R.id.txtInput);
        String name = txt.getText().toString();
        mViewModel.setName(name);

        getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout,
                ThirdFragment.newInstance()).commit();


    }
}