package com.polat.fragment;

import androidx.lifecycle.ViewModel;

public class MainViewModel extends ViewModel {
    String name= "";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
